﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#region Physics States and Type Definitions
public abstract class PhysicsState
{
	public abstract void Enter();
	public abstract void Update();
	public abstract void Exit();
}

public enum PhysicsStateType
{
	Grounded,
	Rolling
}
#endregion

public sealed class ActorPhysics : MonoBehaviour 
{
	//Transform _transform;
	Rigidbody _rigidbody;

	#region States
	public class DefaultState : PhysicsState
	{
		public override void Enter()  {}
		public override void Update() {}
		public override void Exit()   {}
	}
	public delegate void StateCallback(PhysicsStateType physicsStateType);
	public StateCallback ExitStateCallback = delegate {};
	public StateCallback EnterStateCallback = delegate {};

	PhysicsState _currentState = new DefaultState();
	Dictionary<PhysicsStateType, PhysicsState> _stateMap = new Dictionary<PhysicsStateType, PhysicsState>();

	PhysicsStateType _currentStateType = PhysicsStateType.Grounded;
	public PhysicsStateType currentStateType
	{
		get{ return _currentStateType; }
	}
	#endregion

	#region Movement
	[Header("Movement")]
	Vector3 _moveVector = Vector3.zero;

	[SerializeField] float _stoppingSpeed = 0.001f;
	[SerializeField] float _groundedMoveSpeed = 6f;
	public float groundedMoveSpeed 
	{ 
		get { return _groundedMoveSpeed; } 
	}
	[SerializeField] float _rollingMoveSpeed = 15f;
	public float rollingMoveSpeed
	{
		get { return _rollingMoveSpeed; }
	}
	[SerializeField] float _maxRollingTime = 0.5f;
	public float maxRollingTime
	{
		get { return _maxRollingTime; }
	}
	#endregion

	void Awake()
	{
		//_transform = GetComponent<Transform>();
		_rigidbody = GetComponent<Rigidbody>();

		InitializeStateMethodMap();
	}

	void Start()
	{
		ChangeState( PhysicsStateType.Grounded );
	}

	void FixedUpdate()
	{
		_currentState.Update();
	}

	#region State Methods
	void InitializeStateMethodMap()
	{
		foreach (PhysicsStateType state in System.Enum.GetValues(typeof(PhysicsStateType)))
		{
			if (!_stateMap.ContainsKey(state))
			{
				_stateMap.Add(state, new DefaultState());
			}
		}
	}

	public void RegisterState(PhysicsStateType state, PhysicsState method)
	{
		if (_stateMap.ContainsKey(state))
		{
			_stateMap[state] = method;
		}
		else
		{
			_stateMap.Add(state, method);
		}
	}

	public void ChangeState(PhysicsStateType toState)
	{
		_currentState.Exit();
		ExitStateCallback(_currentStateType);
		
		_currentStateType = toState;
		_currentState = _stateMap[_currentStateType];
		
		_currentState.Enter();
		EnterStateCallback(_currentStateType);
	}
	#endregion

	public void GroundMovement(Vector3 moveVector)
	{
		if (moveVector == Vector3.zero)
		{
			ComeToStop();
		}
		else
		{
			MoveAtSpeed(moveVector, groundedMoveSpeed);
		}
	}

	public void RollMovement(Vector3 moveVector)
	{
		MoveAtSpeed(moveVector, rollingMoveSpeed);
	}

	void MoveAtSpeed(Vector3 moveDirection, float moveSpeed)
	{
		_moveVector = moveDirection;

		_moveVector = moveDirection * moveSpeed;
		_moveVector.y = _rigidbody.velocity.y;

		_rigidbody.velocity = _moveVector;
	}

	void ComeToStop()
	{
		_moveVector = _rigidbody.velocity * _stoppingSpeed;
		
		_moveVector.y = _rigidbody.velocity.y;
		_rigidbody.velocity = _moveVector;
	}
}
