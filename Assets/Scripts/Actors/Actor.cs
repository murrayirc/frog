﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ActorPhysics))]
public class Actor : MonoBehaviour 
{
	ActorPhysics _actorPhysics;
	public ActorPhysics physics
	{
		get { return _actorPhysics; }
	}

	public virtual void Awake()
	{
		_actorPhysics = GetComponent<ActorPhysics>();
	}
}
