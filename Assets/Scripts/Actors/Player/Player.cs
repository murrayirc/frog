﻿using UnityEngine;
using System.Collections;

public class Player : Actor 
{
	PlayerControls _controls;
	public PlayerControls controls
	{
		get { return _controls; }
	}

	public override void Awake()
	{
		base.Awake();

		_controls = GetComponent<PlayerControls>();
	}
}
