﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCamera : MonoBehaviour 
{
	[ReadOnly] public Camera cam;
	[ReadOnly, SerializeField] Enemy _lockTarget;

	[Header("Focus")]
	// This is what the camera looks at.
	// When locked on, it will move to the center point between the player and the target.
	// Otherwise it focuses on the center of the screen.
	Vector3 _focusPoint = Vector3.zero;
	Vector3 _focusOffset = Vector3.zero;
	[SerializeField] float _focusFollowSpeed = 12f;

	[Header("Bounding Circle")]
	[ReadOnly, SerializeField] Vector2 _center;
	[SerializeField] float _radius = 100;
	Vector3 _circleOffset = Vector3.zero;

	enum CameraState
	{
		DEFAULT = 0,
		LOCK_ON = 1
	}
	CameraState camState;

	void Awake()
	{
		//Cursor.lockState = CursorLockMode.Locked;
		//Cursor.visible = false;

		cam = Camera.main;
		_lockTarget = null;

		_center = new Vector2(Screen.width / 2, Screen.height / 2);

		_focusPoint = transform.position;
		_circleOffset = _focusPoint;
		_focusOffset = cam.transform.position - _focusPoint;

		camState = CameraState.DEFAULT;
	}

	void FixedUpdate()
	{
		CameraControl();
	}

	void Update()
	{
		CameraInput();
	}

	void CameraInput()
	{
		if (Input.GetKeyDown(KeyCode.L))
		{
			CalculateLockOnTarget();
			if (_lockTarget)
			{
				camState = camState == CameraState.DEFAULT ? CameraState.LOCK_ON : CameraState.DEFAULT;
				Debug.Log ("Changing Camera State to " + camState);
			}
		}
	}

	void CameraControl()
	{
		if (camState == CameraState.DEFAULT)
		{
			_focusPoint = CheckBoundingCircle();
		}
		else
		{
			_focusPoint = LockOnPoint();
		}

		cam.transform.position = Vector3.Lerp (cam.transform.position, _focusPoint + _focusOffset, _focusFollowSpeed * Time.deltaTime);
	}

	Vector3 CheckBoundingCircle()
	{
		Vector2 playerScreenPoint = cam.WorldToScreenPoint(transform.position);
		Vector2 distance = playerScreenPoint - _center;

		if (Mathf.Pow((distance.x), 2) + Mathf.Pow((distance.y), 2) > Mathf.Pow(_radius, 2))
		{
			// Is outside of the circle.
			Vector3 retVec = new Vector3(transform.position.x - _circleOffset.x, _focusPoint.y, transform.position.z - _circleOffset.z);
			return retVec;
		}
		else
		{
			_circleOffset = new Vector3(transform.position.x - _focusPoint.x, _focusPoint.y, transform.position.z - _focusPoint.z);
			return _focusPoint;
		}
	}

	Vector3 LockOnPoint()
	{
		Vector3 distance = _lockTarget.transform.position - transform.position;
		Vector3 retVec = new Vector3(transform.position.x + (distance.x / 2), _focusPoint.y, transform.position.z + (distance.z / 2));
		return retVec;
	}

	void CalculateLockOnTarget()
	{
		Object[] list = FindObjectsOfType(typeof(Enemy));
		List<Enemy> enemies = new List<Enemy>();
		
		foreach (Enemy e in list)
		{
			enemies.Add(e);
		}
		
		Vector3 minDistance = enemies[0].transform.position - transform.position;
		_lockTarget = enemies[0];
		
		foreach (Enemy e in enemies)
		{
			Vector3 distance = e.transform.position - transform.position;
			if (Vector3.Magnitude(distance) < Vector3.Magnitude(minDistance))
			{
				minDistance = distance;
				_lockTarget = e;
			}
		}
	}
}




















