﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ActorPhysics), typeof(Player))]
public class PlayerControls : MonoBehaviour 
{
	Player _player;

	void Start()
	{
		_player = GetComponent<Player>();

		SetupStateMethodMap();
	}

	void FixedUpdate()
	{
		// Update buttons
		// TODO: Add a button input system that updates in Update and not Fixedupdate. Check godlands.
	}

	#region Physics States
	void SetupStateMethodMap()
	{
		_player.physics.RegisterState(PhysicsStateType.Grounded, new Grounded(_player));
		_player.physics.RegisterState(PhysicsStateType.Rolling, new Rolling(_player));
	}

	public class Grounded : PhysicsState
	{
		Player player;
		public Grounded(Player player)
		{
			this.player = player;
		}

		public override void Enter() {}

		public override void Update()
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				//player.animator.SetTrigger("roll");
				player.physics.ChangeState(PhysicsStateType.Rolling);
				return;
			}

			Vector3 moveVector = player.controls.GetMoveInput();
			moveVector = Camera.main.transform.TransformDirection(moveVector);
			moveVector.y = 0;
			player.physics.GroundMovement(moveVector);
		}

		public override void Exit() {}
	}

	public class Rolling : PhysicsState
	{
		Player player;
		public Rolling(Player player)
		{
			this.player = player;
		}

		float _rollTimer;

		public override void Enter() 
		{
			_rollTimer = 0;
		}
		
		public override void Update()
		{
			_rollTimer += Time.deltaTime;

			if (_rollTimer >= player.physics.maxRollingTime)
			{
				player.physics.ChangeState(PhysicsStateType.Grounded);
			}

			Vector3 moveVector = player.controls.GetMoveInput();
			moveVector = Camera.main.transform.TransformDirection(moveVector);
			moveVector.y = 0;
			player.physics.RollMovement(moveVector);
		}
		
		public override void Exit() 
		{
			// player.animator.ResetTrigger("roll");
		}

	}
	#endregion

	Vector3 GetMoveInput()
	{
		return new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
	}
}
